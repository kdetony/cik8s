# Deployment CI Gitlab to Digital Ocean Kubernetes

Repositorio base para realizar CI 

## Getting started

- Contar una cuenta en Gitlab.
- Contar con una cuenta en DO y tener un cluster de Kubernetes creado. 

## Links 

- [ ] [Crear Cuenta DigitalOcean](https://m.do.co/c/35f14306ae1c)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:2da09c038d222d930dab5b4b8d6a337a?https://www.makeareadme.com/) for this template.

